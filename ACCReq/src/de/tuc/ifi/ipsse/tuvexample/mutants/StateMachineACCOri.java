package de.tuc.ifi.ipsse.tuvexample.mutants;

import de.tuc.ifi.ipsse.tuvexample.acc_statemachine.Actuator;
import de.tuc.ifi.ipsse.tuvexample.acc_statemachine.DistanceWarning;
import de.tuc.ifi.ipsse.tuvexample.acc_statemachine.DriverInput;
import de.tuc.ifi.ipsse.tuvexample.acc_statemachine.OnDistance;
import de.tuc.ifi.ipsse.tuvexample.acc_statemachine.OnSpeed;
import de.tuc.ifi.ipsse.tuvexample.acc_statemachine.State;

public class StateMachineACCOri extends ACC implements State{

    private final OnSpeed speed = new OnSpeed();   
    private final OnDistance distance = new OnDistance();    
	private final DistanceWarning distWarning = new DistanceWarning();   
    private final DriverInput driver = new DriverInput();  
    private final Actuator actuator = new Actuator();


    
    
	@Override
	public void step(double DTinSec) {
		// TODO Auto-generated method stub
		
		if(this.inAcceleratorPedal!=0.0 || this.inBreakPedal!=0.0 || !this.inActive ){  		//  R[1]
			passVelocity(state);
			state = States.OFF;
		}
		switch(state){
    	case OFF:
//    		System.out.println("off branch");
    		if(!this.inActive || this.inAcceleratorPedal!=0.0 || this.inBreakPedal!=0.0){					// [R1]
    			turnOff();	
    			
    			passVelocity(state);
    			
    			state = States.OFF;
    			break;
    		}
    		else if(this.inActive && !speed.isEngaged && this.inBreakPedal==0.0 && this.inAcceleratorPedal==0.0){	// R[2]
//    			updateSpeed();	
    			setOn(); 	
    			passVelocity(state);
            	state = States.ON_SPEED;      	
            	break;
            }else 
            	break;
    		
    	case ON_SPEED:
    		updateSpeed();
//    		System.out.println("speed branch");
    		
    		if(this.inObjectInFront){ 			//  R[5]
    			updateWarning();	
    		}

    		if (this.inObjectInFront && this.inObjectDistance < distance.cruiseDistance) {	// R[7]
    			passVelocity(state);
//    			System.out.println("Übergang Dist: d_to: "+ inObjectDistance + " < d_cruise:"+ distance.cruiseDistance );
    			state = States.ON_DISTANCE;
            	break;
            } else {         											// R[8]

            	speed.step(DTinSec);

            	passVelocity(state);
            	
            	state = States.ON_SPEED;
            	break;
            }
    		
    	case ON_DISTANCE:
    		updateDistance();
//    		System.out.println("dist branch");
    		if(this.inObjectInFront){ 			//  R[5]
    			updateWarning();	
    		}

    		if(!distance.inObjectInFront){		// R[9]
    			speed.outCCVelocity = distance.outDistVelocity;
    			passVelocity(state);
    			state = States.ON_SPEED;
    			break;
    		}		

    		if(distance.inObjectInFront && distance.inObjectDistance < distance.cruiseDistance){		// R[8]
    			distance.step(DTinSec);
    			passVelocity(state);
                state = States.ON_DISTANCE;
    			break;
    		}else{												// R[10]
    			speed.outCCVelocity = distance.outDistVelocity;
    			passVelocity(state);
    			state = States.ON_SPEED;
    			
    			break;
    		}
    		

    		
    	default:
    		break;
    	}
		
		
		// DEBUG
//		System.out.println("---- ACC Zustand:"+state +" gas:"+inAcceleratorPedal+ " bremse:"+inBreakPedal+ " activeBtn:"+inActive +" outActive/Status:"+outActive);
//		System.out.println("Zustand:"+state);
		
		
		
		
		
		
		driver.inAcceleratorPedal = this.inAcceleratorPedal;
        driver.inBreakPedal = this.inBreakPedal;
        driver.inCurrentVelocity = this.inCurrentVelocity;
        driver.step(DTinSec);
                
        // compute actuator

//        actuator.inACCVelocity = modeLogic.outACCVelocity;			
        actuator.inDriverVelocity = driver.outDriverVelocity;
        actuator.inCurrentVelocity = this.inCurrentVelocity;
        actuator.step(DTinSec);
        
       
        
        this.outVelocity = actuator.outDesiredVelocity;
//        this.outActive = modeLogic.outActive;
        this.outWarning = distWarning.outWarning;
	}

	//setzt die inputs für speed
    public void updateSpeed(){
    	speed.inCurrentVelocity = this.inCurrentVelocity;
    	speed.inActive = this.inActive;
    }
    
    //setzt die inputs für distance
    public void updateDistance(){
    	distance.inCCVelocity = speed.outCCVelocity;
    	distance.inObjectDistance = this.inObjectDistance;
    	distance.inObjectRelativeVelocity = this.inObjectRelativeVelocity;
    	distance.inObjectInFront = this.inObjectInFront;
    	distance.inCurrentVelocity = this.inCurrentVelocity;
    }
    // setzt die inputs für die warning
    public void updateWarning(){
    	distWarning.inObjectDistance = this.inObjectDistance;
        distWarning.inObjectRelativeVelocity = this.inObjectRelativeVelocity;
        distWarning.inObjectInFront = this.inObjectInFront;
        distWarning.inCurrentVelocity = this.inCurrentVelocity;      
        distWarning.calcWarning();
    }
    
    // um beim Zustandsübergang von Distance in Speed Geschwindigkeitssprünge zu vermeiden
    public void distanceToSpeed(){
    	speed.outCCVelocity = distance.outDistVelocity;
    }
    
    // Schaltet die Geschwindigkeitsregelung ein und setzt Soll-Geschwindigkeit und Abstand
    public void setOn(){

    	speed.targetVelocity = inCurrentVelocity;	
    	speed.outCCVelocity = inCurrentVelocity;
		speed.isEngaged = true;
		distance.cruiseDistance = (speed.targetVelocity*3.6)/2 ;
		v_cruise = inCurrentVelocity;
		d_cruise = distance.cruiseDistance;
    }
    // deaktiviert die Geschwindigkeitsregelung
    public void turnOff(){
    	speed.isEngaged = false;
    }
    
    //gibt die korrekte Geschwindigkeit an die äußeren Komponenten weiter
    public void passVelocity(States state){
    	switch(state){
    	case OFF:
    		this.outActive = this.inActive && (this.inBreakPedal == 0.0) && this.inAcceleratorPedal == 0.0;
    		actuator.inACCVelocity = this.inCurrentVelocity;
    		break;
    	case ON_SPEED:
    		this.outActive = this.inActive && (this.inBreakPedal == 0.0) && this.inAcceleratorPedal == 0.0;
    		actuator.inACCVelocity = speed.outCCVelocity;
    		break;
    	case ON_DISTANCE:
    		this.outActive = this.inActive && (this.inBreakPedal == 0.0) && this.inAcceleratorPedal == 0.0;
    		actuator.inACCVelocity = distance.outDistVelocity;
    		break;
    	}
    }
    
    public OnSpeed getSpeed() {
		return speed;
	}

	public OnDistance getDistance() {
		return distance;
	}



    public double getCCVelocity() {
        return speed.targetVelocity;
    }
}
