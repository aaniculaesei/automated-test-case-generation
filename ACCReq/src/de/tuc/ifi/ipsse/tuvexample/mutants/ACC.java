package de.tuc.ifi.ipsse.tuvexample.mutants;



public abstract class ACC {
	public boolean inActive = false;

	public double inCurrentVelocity = 0.0;

	public double inBreakPedal = 0.0;

	public double inAcceleratorPedal = 0.0;

	public double inObjectDistance = 0.0;

	public double inObjectRelativeVelocity = 0.0;

	public boolean inObjectInFront = false;

	public double outVelocity = 0.0;

	public boolean outActive = false;

	public boolean outWarning = false;
    
    public enum States 
    {
        OFF,
        ON_SPEED,
        ON_DISTANCE;

    }

    public States state = States.OFF;

    public double v_cruise = 0.0;

    public double d_cruise = 0.0;

    public abstract void step( double DTinSec );
    
    public abstract de.tuc.ifi.ipsse.tuvexample.acc_statemachine.OnDistance getDistance();
    
    public abstract de.tuc.ifi.ipsse.tuvexample.acc_statemachine.OnSpeed getSpeed();
    
    public abstract void updateDistance();
    
    public abstract void updateWarning();
}
