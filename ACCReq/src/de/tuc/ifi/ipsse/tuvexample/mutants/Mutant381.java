// This is a mutant program.
// Author : ysma

package de.tuc.ifi.ipsse.tuvexample.mutants;


public class Mutant381 extends ACC implements de.tuc.ifi.ipsse.tuvexample.acc_statemachine.State
{





















    private final de.tuc.ifi.ipsse.tuvexample.acc_statemachine.OnSpeed speed = new de.tuc.ifi.ipsse.tuvexample.acc_statemachine.OnSpeed();

    private final de.tuc.ifi.ipsse.tuvexample.acc_statemachine.OnDistance distance = new de.tuc.ifi.ipsse.tuvexample.acc_statemachine.OnDistance();

    private final de.tuc.ifi.ipsse.tuvexample.acc_statemachine.DistanceWarning distWarning = new de.tuc.ifi.ipsse.tuvexample.acc_statemachine.DistanceWarning();

    private final de.tuc.ifi.ipsse.tuvexample.acc_statemachine.DriverInput driver = new de.tuc.ifi.ipsse.tuvexample.acc_statemachine.DriverInput();

    private final de.tuc.ifi.ipsse.tuvexample.acc_statemachine.Actuator actuator = new de.tuc.ifi.ipsse.tuvexample.acc_statemachine.Actuator();














public Mutant381(){};
    public  void step( double DTinSec )
    {
        if (++this.inAcceleratorPedal != 0.0 || this.inBreakPedal != 0.0 || !this.inActive) {
            passVelocity( state );
            state = States.OFF;
        }
        switch (state) {
        case OFF :
            if (!this.inActive || this.inAcceleratorPedal != 0.0 || this.inBreakPedal != 0.0) {
                turnOff();
                passVelocity( state );
                state = States.OFF;
                break;
            } else {
                if (this.inActive && !speed.isEngaged && this.inBreakPedal == 0.0 && this.inAcceleratorPedal == 0.0) {
                    setOn();
                    passVelocity( state );
                    state = States.ON_SPEED;
                    break;
                } else {
                    break;
                }
            }

        case ON_SPEED :
            updateSpeed();
            if (this.inObjectInFront) {
                updateWarning();
            }
            if (this.inObjectInFront && this.inObjectDistance < distance.cruiseDistance) {
                passVelocity( state );
                state = States.ON_DISTANCE;
                break;
            } else {
                speed.step( DTinSec );
                passVelocity( state );
                state = States.ON_SPEED;
                break;
            }

        case ON_DISTANCE :
            updateDistance();
            if (this.inObjectInFront) {
                updateWarning();
            }
            if (!distance.inObjectInFront) {
                speed.outCCVelocity = distance.outDistVelocity;
                passVelocity( state );
                state = States.ON_SPEED;
                break;
            }
            if (distance.inObjectInFront && distance.inObjectDistance < distance.cruiseDistance) {
                distance.step( DTinSec );
                passVelocity( state );
                state = States.ON_DISTANCE;
                break;
            } else {
                speed.outCCVelocity = distance.outDistVelocity;
                passVelocity( state );
                state = States.ON_SPEED;
                break;
            }

        default  :
            break;

        }
        driver.inAcceleratorPedal = this.inAcceleratorPedal;
        driver.inBreakPedal = this.inBreakPedal;
        driver.inCurrentVelocity = this.inCurrentVelocity;
        driver.step( DTinSec );
        actuator.inDriverVelocity = driver.outDriverVelocity;
        actuator.inCurrentVelocity = this.inCurrentVelocity;
        actuator.step( DTinSec );
        this.outVelocity = actuator.outDesiredVelocity;
        this.outWarning = distWarning.outWarning;
    }

    public  void updateSpeed()
    {
        speed.inCurrentVelocity = this.inCurrentVelocity;
        speed.inActive = this.inActive;
    }

    public  void updateDistance()
    {
        distance.inCCVelocity = speed.outCCVelocity;
        distance.inObjectDistance = this.inObjectDistance;
        distance.inObjectRelativeVelocity = this.inObjectRelativeVelocity;
        distance.inObjectInFront = this.inObjectInFront;
        distance.inCurrentVelocity = this.inCurrentVelocity;
    }

    public  void updateWarning()
    {
        distWarning.inObjectDistance = this.inObjectDistance;
        distWarning.inObjectRelativeVelocity = this.inObjectRelativeVelocity;
        distWarning.inObjectInFront = this.inObjectInFront;
        distWarning.inCurrentVelocity = this.inCurrentVelocity;
        distWarning.calcWarning();
    }

    public  void distanceToSpeed()
    {
        speed.outCCVelocity = distance.outDistVelocity;
    }

    public  void setOn()
    {
        speed.targetVelocity = inCurrentVelocity;
        speed.outCCVelocity = inCurrentVelocity;
        speed.isEngaged = true;
        distance.cruiseDistance = speed.targetVelocity * 3.6 / 2;
        v_cruise = inCurrentVelocity;
        d_cruise = distance.cruiseDistance;
    }

    public  void turnOff()
    {
        speed.isEngaged = false;
    }

public  void passVelocity(States state )
    {
        switch (state) {
        case OFF :
            this.outActive = this.inActive && this.inBreakPedal == 0.0 && this.inAcceleratorPedal == 0.0;
            actuator.inACCVelocity = this.inCurrentVelocity;
            break;

        case ON_SPEED :
            this.outActive = this.inActive && this.inBreakPedal == 0.0 && this.inAcceleratorPedal == 0.0;
            actuator.inACCVelocity = speed.outCCVelocity;
            break;

        case ON_DISTANCE :
            this.outActive = this.inActive && this.inBreakPedal == 0.0 && this.inAcceleratorPedal == 0.0;
            actuator.inACCVelocity = distance.outDistVelocity;
            break;

        }
    }

    public  de.tuc.ifi.ipsse.tuvexample.acc_statemachine.OnSpeed getSpeed()
    {
        return speed;
    }

    public  de.tuc.ifi.ipsse.tuvexample.acc_statemachine.OnDistance getDistance()
    {
        return distance;
    }

    public  double getCCVelocity()
    {
        return speed.targetVelocity;
    }

}
