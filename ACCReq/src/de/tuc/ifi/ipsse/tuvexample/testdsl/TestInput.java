/*
 * Copyright 2016 Clausthal Univesity of Technology.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.tuc.ifi.ipsse.tuvexample.testdsl;

/**
 *
 * @author falk
 */
public class TestInput {

    /**
     * engage system
     */
    private final boolean inEngage = false;

    /**
     * disengage system
     */
    private final boolean inDisengage = false;
    
    /**
     * current velocity [m/s]
     */
    private final double inCurrentVelocity = 0.0;
    
    /**
     * break pedal pressure [0..1]
     */    
    private final double inBreakPedal = 0.0;

    /**
     * accelerator pedal pressure [0..1]
     */    
    private final double inAcceleratorPedal = 0.0;
    
    /** 
     * measured distance to vehicle in front [m]
     */
    private final double inObjectDistance = 0.0;

    /** 
     * measured distance to vehicle in front [m]
     */
    private final double inObjectRelativeVelocity = 0.0;
    
    /** 
     * recognized object in front
     */
    private final boolean inObjectInFront = false;   
    
    
}
