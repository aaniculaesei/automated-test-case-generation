/*
 * Copyright 2016 Clausthal Univesity of Technology.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.tuc.ifi.ipsse.tuvexample.acc_statemachine;

/**
 *
 * @author falk
 */
public class Constants {
       
    /**
     * maximum velocity [m/s]
     */
    public static final double MAX_VELOCITY = 50.0;

    /**
     * maximum acceleration [m/s^2]
     */
    public static final double MAX_ACCELERATION = 2.0;

    /**
     * maximum deceleration [m/s^2]
     */
    public static final double MAX_DECELERATION = 10.0;
    
    /**
     * maximum ACC deceleration [m/s^2]
     */    
    public static final double MAX_ACC_DECELERATION = 2.0;

}
