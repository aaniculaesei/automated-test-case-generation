/*
 * Copyright 2016 Clausthal Univesity of Technology.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.tuc.ifi.ipsse.tuvexample.acc_statemachine;

/**
 *
 * @author falk
 */
public class DriverInput implements State {

    public double inCurrentVelocity;
    public double inBreakPedal;
    public double inAcceleratorPedal;
    public double outDriverVelocity;

    @Override
    public void step(double DTinSec) {

        // [m/s^2]
        final double acceleration;
                
        if (inBreakPedal > 0.0) {
            acceleration = -1.0 * (inBreakPedal * Constants.MAX_DECELERATION);
        } else {
            acceleration = (inAcceleratorPedal * Constants.MAX_ACCELERATION);            
        }

        // [m/s]
        final double deltaVelocity = (acceleration * DTinSec);        
        final double newVelocity = inCurrentVelocity + deltaVelocity;
        
        if (newVelocity < 0.0) {
            outDriverVelocity = 0.0;
        } else {
            if (newVelocity > Constants.MAX_VELOCITY) {
                outDriverVelocity = Constants.MAX_VELOCITY;
            } else {
                outDriverVelocity = newVelocity;
            }
        }
    }

}
