
package de.tuc.ifi.ipsse.tuvexample.acc_statemachine;

/**
 *
 * @author Peer
 */
public interface State {
    
    /**
     * 
     */
    public void step(double DTinSec);
            
}
