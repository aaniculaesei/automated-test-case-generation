package de.tuc.ifi.ipsse.tuvexample.acc_statemachine;


/**
*
* @author peer
*/
public class OnSpeed implements State{
    public boolean inActive = false;
    public double inCurrentVelocity = 0.0;
    public double targetVelocity = 0.0; 
    public boolean isEngaged = false;
    public double outCCVelocity = 0.0;
    public PID pid = new PID();
	@Override
	
	/*
	 * regulate target velocity
	 */
	public void step(double DTinSec) {
		// TODO Auto-generated method stub
		 double value = pid.getValue(inCurrentVelocity, targetVelocity, DTinSec);
	     outCCVelocity += value*DTinSec;
	}
}
