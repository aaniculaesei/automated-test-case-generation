/*
 * Copyright 2016 Clausthal Univesity of Technology.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.tuc.ifi.ipsse.tuvexample.acc_statemachine;


public class DistanceWarning {

    public boolean inObjectInFront = false;
    public double inObjectDistance = 0.0;
    public double inObjectRelativeVelocity = 0.0;
    public double inCurrentVelocity = 0.0;
    public boolean outWarning = false;

    public double safeDistance;

    public void calcWarning(){
    	computeSafeDistance();
        if(inObjectInFront && inObjectDistance < safeDistance){
        	outWarning = true;
        }else{
        	outWarning = false;
        }
    }
    
    public void computeSafeDistance(){
    	safeDistance = inCurrentVelocity*3.6/4;
    }

   
}
