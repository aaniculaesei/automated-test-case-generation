package de.tuc.ifi.ipsse.tuvexample.acc_statemachine;

public class PID {
	double kp = 1.0f;
	double ki = 0.02f;
	double kd = 0.02f;
	double integral = 0;
	double lastError = 0;
	boolean firstUse = true;
	public double targetValue = 0;
	public double getValue(double currentValue, double targetValue, double dt){
		this.targetValue = targetValue;
		double error = targetValue - currentValue;
		double result = kp*error;	
		integral += error*dt;
		result += integral*ki;
		
		if(!firstUse){
			double derivative = (error - lastError)/dt;
			result += derivative*kd;
		}else{
			firstUse=false;
		}

		lastError = error;
		
		
		return result;
	}


}
