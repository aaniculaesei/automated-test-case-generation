package de.tuc.ifi.ipsse.tuvexample.acc_statemachine;


public class OnDistance implements State{
    public boolean inObjectInFront = false;
    public double inObjectDistance = 0.0;
    public double inObjectRelativeVelocity = 0.0;
    public double inCurrentVelocity = 0.0;
    public double inCCVelocity = 0.0;   
    public double outDistVelocity = 0.0; 
    
    public double cruiseDistance = 0.0; 
    public double cruiseDistVIF = 0.0;
    public PID pid = new PID();
    private boolean firstDist = true;
    public double vifVelocity = 0.0;

	@Override
	public void step(double DTinSec) {
		// TODO Auto-generated method stub

		// avoid velocity error if state is visited the first time
		if(firstDist){
			outDistVelocity = inCurrentVelocity;
			firstDist=false;
		}
		
		

    	if(inObjectDistance < cruiseDistance ){	
    		vifVelocity = inCurrentVelocity + inObjectRelativeVelocity;
    		cruiseDistVIF = (vifVelocity*3.6)/2 ;
    		
    		// if distance to front vehicle is big enough -> regulate front velocity
    		if(inObjectDistance > cruiseDistVIF){
        		double value = pid.getValue(inCurrentVelocity, vifVelocity, DTinSec);
            	outDistVelocity += value*DTinSec;
    		}else{	//if distance is to small -> break until distance is big enough
    			double value = pid.getValue(inCurrentVelocity, vifVelocity*0.85, DTinSec);
            	outDistVelocity += value*DTinSec;
    		}
    		
    		
    		


    	}
	}

	public double getVIFDist(){
		double vifVelocity = inCurrentVelocity + inObjectRelativeVelocity;
		return (vifVelocity*3.6)/2 ;
	}


	public void keepVelocity(){
		outDistVelocity = inCurrentVelocity;
    }

}
