package de.tuc.ifi.ipsse.tuvexample.acc_statemachine;

public class StateMachineACC implements State{
	public boolean inActive = false;
    public double inCurrentVelocity = 0.0;
    public double inBreakPedal = 0.0;
    public double inAcceleratorPedal = 0.0;    
    public double inObjectDistance = 0.0;
    public double inObjectRelativeVelocity = 0.0;    
    public boolean inObjectInFront = false;
    public double outVelocity = 0.0;
    public boolean outActive = false;
    public boolean outWarning = false;
    
    private final OnSpeed speed = new OnSpeed();   
    private final OnDistance distance = new OnDistance();    

	private final DistanceWarning distWarning = new DistanceWarning();   
    private final DriverInput driver = new DriverInput();  
    private final Actuator actuator = new Actuator();

	public enum States {OFF,ON_SPEED,ON_DISTANCE};
    public States state = States.OFF;

    public double v_cruise = 0.0;
    public double d_cruise = 0.0;

    
    
	@Override
	public void step(double DTinSec) {
		
		if(this.inAcceleratorPedal!=0.0 || this.inBreakPedal!=0.0 || !this.inActive ){  		
			passVelocity(state);
			state = States.OFF;			
		}
		switch(state){
    	case OFF:
    		if(!this.inActive || this.inAcceleratorPedal!=0.0 || this.inBreakPedal!=0.0){					
    			turnOff();	   			
    			passVelocity(state);
    			state = States.OFF;
    			break;
    		}
    		else if(this.inActive && !speed.isEngaged && this.inBreakPedal==0.0 && this.inAcceleratorPedal==0.0){	

    			setOn(); 	
    			passVelocity(state);
            	state = States.ON_SPEED;      	
            	break;
            }else 
            	break;
    		
    	case ON_SPEED:
    		updateSpeed();
    		if(this.inObjectInFront){ 			
    			updateWarning();	
    		}
    		if (this.inObjectInFront && this.inObjectDistance < distance.cruiseDistance) {
    			passVelocity(state);
    			state = States.ON_DISTANCE;
            	break;
            } else {         											
            	speed.step(DTinSec);
            	passVelocity(state);            	
            	state = States.ON_SPEED;
            	break;
            }
    		
    	case ON_DISTANCE:
    		updateDistance();
    		if(this.inObjectInFront){ 			
    			updateWarning();	
    		}
    		if(!distance.inObjectInFront){		
    			speed.outCCVelocity = distance.outDistVelocity;
    			passVelocity(state);
    			state = States.ON_SPEED;
    			break;
    		}		

    		if(distance.inObjectInFront && distance.inObjectDistance < distance.cruiseDistance){		
    			distance.step(DTinSec);
    			passVelocity(state);
                state = States.ON_DISTANCE;
    			break;
    		}else{											
    			speed.outCCVelocity = distance.outDistVelocity;
    			passVelocity(state);
    			state = States.ON_SPEED;    			
    			break;
    		}

    		
    	default:
    		break;
    	}
		
	
		driver.inAcceleratorPedal = this.inAcceleratorPedal;
        driver.inBreakPedal = this.inBreakPedal;
        driver.inCurrentVelocity = this.inCurrentVelocity;
        driver.step(DTinSec);
                		
        actuator.inDriverVelocity = driver.outDriverVelocity;
        actuator.inCurrentVelocity = this.inCurrentVelocity;
        actuator.step(DTinSec);
        
       
        
        this.outVelocity = actuator.outDesiredVelocity;
        this.outWarning = distWarning.outWarning;
	}

    public void updateSpeed(){
    	speed.inCurrentVelocity = this.inCurrentVelocity;
    	speed.inActive = this.inActive;
    }
    
    public void updateDistance(){
    	distance.inCCVelocity = speed.outCCVelocity;
    	distance.inObjectDistance = this.inObjectDistance;
    	distance.inObjectRelativeVelocity = this.inObjectRelativeVelocity;
    	distance.inObjectInFront = this.inObjectInFront;
    	distance.inCurrentVelocity = this.inCurrentVelocity;
    }
    public void updateWarning(){
    	distWarning.inObjectDistance = this.inObjectDistance;
        distWarning.inObjectRelativeVelocity = this.inObjectRelativeVelocity;
        distWarning.inObjectInFront = this.inObjectInFront;
        distWarning.inCurrentVelocity = this.inCurrentVelocity;      
        distWarning.calcWarning();
    }
    
    public void distanceToSpeed(){
    	speed.outCCVelocity = distance.outDistVelocity;
    }
    
    public void setOn(){
    	speed.targetVelocity = inCurrentVelocity;	
    	speed.outCCVelocity = inCurrentVelocity;
		speed.isEngaged = true;
		distance.cruiseDistance = (speed.targetVelocity*3.6)/2 ;
		v_cruise = inCurrentVelocity;
		d_cruise = distance.cruiseDistance;
    }
    public void turnOff(){
    	speed.isEngaged = false;
    }
    
    public void passVelocity(States state){
    	switch(state){
    	case OFF:
    		this.outActive = this.inActive && (this.inBreakPedal == 0.0) && this.inAcceleratorPedal == 0.0;
    		actuator.inACCVelocity = this.inCurrentVelocity;
    		break;
    	case ON_SPEED:
    		this.outActive = this.inActive && (this.inBreakPedal == 0.0) && this.inAcceleratorPedal == 0.0;
    		actuator.inACCVelocity = speed.outCCVelocity;
    		break;
    	case ON_DISTANCE:
    		this.outActive = this.inActive && (this.inBreakPedal == 0.0) && this.inAcceleratorPedal == 0.0;
    		actuator.inACCVelocity = distance.outDistVelocity;
    		break;
    	}
    }
    
    public OnSpeed getSpeed() {
		return speed;
	}

	public OnDistance getDistance() {
		return distance;
	}



    public double getCCVelocity() {
        return speed.targetVelocity;
    }
}
