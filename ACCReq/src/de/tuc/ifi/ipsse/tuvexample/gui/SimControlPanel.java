/*
 * Copyright 2016 Clausthal Univesity of Technology.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.tuc.ifi.ipsse.tuvexample.gui;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;

/**
 *
 * @author falk
 */
class SimControlPanel extends JPanel {

    private Mode mode = Mode.PAUSE;

    private boolean hasCar = false;

    private double distInM = 0.0;

    private double velocity = 0.0;

    private final JTextField distField = new JTextField(5);
    private final JTextField velField = new JTextField(5);

    SimControlPanel() {
        initialize();
    }

    private void initialize() {

        this.setLayout(new FlowLayout(FlowLayout.CENTER));

        final JButton step = new JButton("Step");
        step.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mode = Mode.STEP;
            }
        });

        final JButton play = new JButton("Play");
        play.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mode = Mode.PLAY;
            }
        });

        final JButton pause = new JButton("Pause");
        pause.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mode = Mode.PAUSE;
            }
        });

        add(step);
        add(play);
        add(pause);

        add(new JSeparator(JSeparator.VERTICAL));

        add(new JLabel("Dist [m]:"));
        add(distField);
        add(new JLabel("Vel [km/h]:"));
        add(velField);
        
        final JButton setCar = new JButton("+");
        setCar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                distField.setEditable(false);
                if (hasCar) {
                    try {
                        velocity = Double.parseDouble(velField.getText());
                    } catch (NumberFormatException ex) {
                        // do nothing
                    }
                } else {
                    hasCar = true;
                    try {
                        velocity = Double.parseDouble(velField.getText());
                        distInM = Double.parseDouble(distField.getText());
                    } catch (NumberFormatException ex) {
                        // do nothing
                    }                    
                }
            }
        });
        
        final JButton delCar = new JButton("-");
        delCar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                hasCar = false;
                distField.setEditable(true);
            }
        });
        
        add(setCar);
        add(delCar);
    }

    Mode getMode() {
        if (mode == Mode.STEP) {
            mode = Mode.PAUSE;
            return Mode.STEP;
        }
        return mode;
    }

    boolean hasCar() {
        return hasCar;
    }

    double getDistance() {
        return distInM;
    }

    double getVelocity() {
        return velocity;
    }

    void setHasCar(boolean hasCar) {
        this.hasCar = hasCar;
        distField.setEditable(!hasCar);        
    }

    final void update() {
        repaint();
    }    

}
