/*
 * Copyright 2016 Clausthal Univesity of Technology.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.tuc.ifi.ipsse.tuvexample.gui;

import java.awt.FlowLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;

/**
 *
 * @author falk
 */
class CarControlsPanel extends JPanel {
        
    private JSlider accelerator;

    private JSlider decelerator;
    
    private JCheckBox accOn;
    
    public CarControlsPanel() {
        initialize();
    }
    
    private void initialize() {
        
        setLayout(new FlowLayout(FlowLayout.CENTER));
                
        accelerator = new JSlider(JSlider.HORIZONTAL, 0, 100, 0);
        decelerator = new JSlider(JSlider.HORIZONTAL, 0, 100, 0);
        
        add(decelerator);        
        add(new JLabel("Brake"));
        add(accelerator);
        add(new JLabel("Accel."));
        
        accOn = new JCheckBox("ACC");        
        add(accOn);
    }
        
    void update() {
        repaint();
    }

    void setAccStatus(boolean state) {
        this.accOn.setSelected(state);
    }
    
    boolean getAccStatus() {
        return this.accOn.isSelected();
    }
    
    double getAcceleration() {
        return ((double) this.accelerator.getValue()) / 100.0 ;
    }

    double getDeceleration() {
        return ((double) this.decelerator.getValue()) / 100.0 ;
    }    
}
