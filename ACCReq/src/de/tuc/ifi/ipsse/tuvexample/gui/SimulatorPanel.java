/*
 * Copyright 2016 Clausthal Univesity of Technology.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.tuc.ifi.ipsse.tuvexample.gui;

import java.awt.BorderLayout;
import javax.swing.JPanel;

/**
 *
 * @author falk
 */
public class SimulatorPanel extends JPanel {
        
    private InfoPanel info;
    
    private CarControlsPanel carControls;
    
    private SimControlPanel simControls;
    
    private RoadVisualizationPanel road; 
    
    public SimulatorPanel() {
        initialize();
    }
    
    private void initialize() {
        
        info = new InfoPanel();
        carControls = new CarControlsPanel();
        simControls = new SimControlPanel();
        road = new RoadVisualizationPanel();
        
        setLayout(new BorderLayout());
        
        add(simControls, BorderLayout.NORTH);
        add(road, BorderLayout.CENTER);
        add(carControls, BorderLayout.SOUTH);
        add(info, BorderLayout.EAST);        
    }

    public boolean hasCar() {
        return simControls.hasCar();
    }
    
    public void setHasCar(boolean hasCar) {
        simControls.setHasCar(hasCar);
    }

    public double getAcceleration() {
        return carControls.getAcceleration();
    }

    public double getDeceleration() {
        return carControls.getDeceleration();
    } 
    
    public Mode getMode() {
        return simControls.getMode();
    }

    public boolean getAccActive() {
        return carControls.getAccStatus();
    }

    public void setAccActive(boolean outActive) {
        carControls.setAccStatus(outActive);
        info.setAccState(outActive);
    }

    public void setAccSpeed(double ccVelocity) {
        info.setACCSpeed(ccVelocity);
    }

    public void setWarning(boolean outWarning) {
        info.setWarning(outWarning);
    }

    public void setVelocity(double egoVelocityInMS) {
        info.setCurrentSpeed(egoVelocityInMS);
    }

    public void setEgoPos(double egoPosInM) {
        road.setEgoPos(egoPosInM);
    }
    
    public void update() {
        info.update();
        carControls.update();
        simControls.update();
        road.update();
    }

    public double getDistanceInM() {
        return simControls.getDistance();
    }

    public double getCarVelocityInMS() {
        return simControls.getVelocity() * 1000.0 / 3600.0;
    }

    public void setCarPos(double carPosInM) {
        road.setCarPos(carPosInM);
    }

    public void setDrawCar(boolean draw) {
        road.setDrawCar(draw);
    }
}
