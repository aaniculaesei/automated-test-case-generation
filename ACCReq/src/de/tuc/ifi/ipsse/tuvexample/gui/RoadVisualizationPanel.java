/*
 * Copyright 2016 Clausthal Univesity of Technology.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.tuc.ifi.ipsse.tuvexample.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import javax.swing.JPanel;

class RoadVisualizationPanel extends JPanel {

    private Rectangle2D ego;    
    private Rectangle2D car;
    private Ellipse2D tree;
    private Rectangle2D ground;
    private Rectangle2D sky;
    
    public final static int PIXEL_HEIGHT = 400;
    
    public final static int PIXEL_WIDTH = 1000;
    
    private final static int GROUND_HEIGHT = 50;
    
    private double egoPosInM = 0;
    
    private double carPosInM = 0;
    
    private final double WIDTH_IN_M = 100.0;
    
    private final double meterToPixel = (double) PIXEL_WIDTH / WIDTH_IN_M;
    
    private boolean drawCar = false;
    
    private final double treeHeightinM = 10.0;
    
    private final int lengthCarInPixel = (int) (4.0 * meterToPixel);
    
    private final int heightCarInPixel = (int) (1.5 * meterToPixel);
        
    private final int heightTreeInPixel = (int) (treeHeightinM * meterToPixel);

    private final double offsetInM = 10;
        
    private final int offsetInPixel = (int) (offsetInM * meterToPixel);
    
    RoadVisualizationPanel() {        
        initialize();
    }
    
    
    private void initialize() {
        
        Dimension d = new Dimension(PIXEL_WIDTH, PIXEL_HEIGHT);
        setSize(d);
        setMinimumSize(d);
        setPreferredSize(d);
        
        ground = new Rectangle2D.Float(0, PIXEL_HEIGHT - GROUND_HEIGHT, PIXEL_WIDTH, GROUND_HEIGHT);
        sky = new Rectangle2D.Float(0, 0, PIXEL_WIDTH, PIXEL_HEIGHT - GROUND_HEIGHT);
        ego = new Rectangle2D.Float(offsetInPixel, PIXEL_HEIGHT - GROUND_HEIGHT - 
                heightCarInPixel -3 , lengthCarInPixel, heightCarInPixel);
                
    }

    private void doDrawing(Graphics g) {

        Graphics2D g2d = (Graphics2D) g.create();

        //rectx = (rectx+10) % 250;        
        //rect.setRect(rectx, 20f, 80f, 50f);
        
        RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        rh.put(RenderingHints.KEY_RENDERING,
                RenderingHints.VALUE_RENDER_QUALITY);
        
        g2d.setRenderingHints(rh);

        g2d.setPaint(new Color(0, 0, 0));        
        g2d.fill(ground);

        g2d.setPaint(new Color(235, 240, 255));        
        g2d.fill(sky);

        double treePosInM = WIDTH_IN_M - (egoPosInM % WIDTH_IN_M);
        int treePosInPixel = (int) (treePosInM * meterToPixel);
        tree = new Ellipse2D.Float(treePosInPixel, PIXEL_HEIGHT - GROUND_HEIGHT - 
                heightTreeInPixel, heightTreeInPixel / 4.0f , heightTreeInPixel);
                
        g2d.setPaint(new Color(0, 200, 190));        
        g2d.fill(tree);
        
        g2d.setPaint(new Color(235, 150, 150));        
        g2d.fill(ego);
        
        if (drawCar) {
            int carPosInPixel = (int) ((carPosInM - egoPosInM) * meterToPixel);
            car = new Rectangle2D.Float(offsetInPixel + carPosInPixel, 
                    PIXEL_HEIGHT - GROUND_HEIGHT - 
                    heightCarInPixel -3 , lengthCarInPixel, heightCarInPixel);        

            g2d.setPaint(new Color(0, 0, 210));        
            g2d.fill(car);
        }
        
        g2d.dispose();
    }

    @Override
    public void paintComponent(Graphics g) {

        super.paintComponent(g);
        doDrawing(g);
    }

    void update() {
        repaint();        
    }
    
    void setDrawCar(boolean state) {
        this.drawCar = state;
    }
    
    void setEgoPos(double inM) {
        this.egoPosInM = inM;
    }
    
    void setCarPos(double inM) {
        this.carPosInM = inM;
    }    
}