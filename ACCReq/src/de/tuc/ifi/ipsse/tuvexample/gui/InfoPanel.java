/*
 * Copyright 2016 Clausthal Univesity of Technology.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.tuc.ifi.ipsse.tuvexample.gui;

import static de.tuc.ifi.ipsse.tuvexample.gui.RoadVisualizationPanel.PIXEL_HEIGHT;
import static de.tuc.ifi.ipsse.tuvexample.gui.RoadVisualizationPanel.PIXEL_WIDTH;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.text.DecimalFormat;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.text.NumberFormatter;

/**
 *
 * @author falk
 */
class InfoPanel extends JPanel {

    private boolean accInfo = false;

    private double accSpeedInMproS = 0.0;

    private double currentSpeedInMproS = 0.0;

    private boolean warning = false;

    private JLabel accLabel;

    private JLabel warnLabel;

    private JLabel speedLabel;

    private static final DecimalFormat formatter = new DecimalFormat(".##");
    
    InfoPanel() {
        initialize();
        update();
    }

    private void initialize() {
        
        Dimension d = new Dimension(300, 200);
        setSize(d);
        setMinimumSize(d);
        setPreferredSize(d);
        
        BoxLayout bl = new BoxLayout(this, BoxLayout.Y_AXIS);
        setLayout(bl);
                
        accLabel = new JLabel();
        speedLabel = new JLabel();
        warnLabel = new JLabel();
        
        Font font = new Font("SansSerif", Font.BOLD, 30);

        accLabel.setFont(font);
        accLabel.setForeground(Color.green);
        accLabel.setVerticalAlignment(SwingConstants.CENTER);

        speedLabel.setFont(font);
        speedLabel.setVerticalAlignment(SwingConstants.CENTER);
        
        warnLabel.setFont(font);
        warnLabel.setForeground(Color.red);
        warnLabel.setVerticalAlignment(SwingConstants.CENTER);
        
        accLabel.setBorder(new EmptyBorder(5, 20, 5, 20));
        speedLabel.setBorder(new EmptyBorder(5, 20, 5, 20));        
        warnLabel.setBorder(new EmptyBorder(5, 20, 5, 20));        
        
        add(accLabel);
        add(speedLabel);
        add(warnLabel);
    }
    
    final void update() {
        
        if (accInfo) {
            accLabel.setText("<html><div style='text-align: center;'>" + 
                    formatter.format(accSpeedInMproS * 3.6) + " KM/H</div></html>");            
            
        } else {
            accLabel.setText("<html><div style='text-align: center;'>OFF</div></html>");
        }
        
        speedLabel.setText("<html><div style='text-align: center;'>" +
                formatter.format(currentSpeedInMproS * 3.6) + " KM/H</div></html>");
        
        warnLabel.setText( warning ? 
                "<html><div style='text-align: center;'>WARN</div></html>" : "");      
        
        repaint();
    }
    
    void setAccState(boolean state) {
        this.accInfo = state;
    }
    
    void setCurrentSpeed(double inMperS) {
        this.currentSpeedInMproS = inMperS;
    }
    
    void setACCSpeed(double inMperS) {
        this.accSpeedInMproS = inMperS;
    }
 
    void setWarning(boolean state) {
        this.warning = state;
    }    
}
