package de.tuc.ifi.ipsse.tuvexample.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import de.tuc.ifi.ipsse.tuvexample.acc_statemachine.StateMachineACC;
import de.tuc.ifi.ipsse.tuvexample.mutants.ACC;

/**
 *	this class is a datastructure which connects the vars of the model checker with the implementation vars
 *  it also contains the test methods for check() and set()
 */
public class Var {
	private ACC acc;
	private boolean initState;	// initial state variable ? 
	private boolean check = false;
	private Object value;
	private String varNameAcc;
	
	public Var(ACC acc, boolean initState, Object value,String varNameAcc) {
		// TODO Auto-generated constructor stub
		this.acc = acc;
		this.initState = initState;
		this.value = value;
		this.varNameAcc = varNameAcc;

	}
	
	/**
	 * checks if impl. variable and model checker variable values are equal
	 */
	@Test
	public void check(){
		try {
			Object varValACC = acc.getClass().getField(varNameAcc).get(acc);
			if(varNameAcc.equals("d_cruise") || varNameAcc.equals("v_cruise") || varNameAcc.equals("inCurrentVelocity") || varNameAcc.equals("inObjectDistance")){
				assertTrue((double)varValACC-(double)value <= 0.001);
			}else{
				assertEquals(""+varNameAcc+" "+varValACC,varValACC, value);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	};
	
	//sets the value of the impl. variable to the value given by the model checker variable
	public void set(){
		try {
			acc.getClass().getField(varNameAcc).set(acc, value);	
		} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
	};
	
	//returns true if variable is an oracle variable
	public boolean isCheck() {
		return check;
	}

	public void setCheck(boolean check) {
		this.check = check;
	}	
	public ACC getAcc() {
		return acc;
	}

	public void setAcc(ACC acc) {
		this.acc = acc;
	}

	public boolean isInitState() {
		return initState;
	}

	public void setInitState(boolean initState) {
		this.initState = initState;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public String getVarNameAcc() {
		return varNameAcc;
	}


	public void setVarNameAcc(String varNameAcc) {
		this.varNameAcc = varNameAcc;
	}
	
	@Override
	public String toString() {
		return "Var [initState=" + initState + ", check=" + check + ", value=" + value + ", varNameAcc=" + varNameAcc
				+ "]";
	}

}
