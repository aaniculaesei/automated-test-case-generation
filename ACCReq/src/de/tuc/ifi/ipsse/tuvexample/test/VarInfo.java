package de.tuc.ifi.ipsse.tuvexample.test;

public class VarInfo {
	private String varNameAcc;
	private Class type;			//type of the variable in the acc implementation
	private boolean initVar;	

	public VarInfo(String varNameAcc, Class type) {
		super();
		this.varNameAcc = varNameAcc;
		this.type = type;

	}
	public String getVarNameAcc() {
		return varNameAcc;
	}
	public void setVarNameAcc(String varNameAcc) {
		this.varNameAcc = varNameAcc;
	}
	public Class getType() {
		return type;
	}
	public void setType(Class type) {
		this.type = type;
	}
	public boolean isInitVar() {
		return initVar;
	}
	public void setInitVar(boolean initVar) {
		this.initVar = initVar;
	}

	
}
