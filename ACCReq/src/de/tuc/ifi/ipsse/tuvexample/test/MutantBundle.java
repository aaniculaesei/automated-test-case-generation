package de.tuc.ifi.ipsse.tuvexample.test;

import java.util.ArrayList;

import de.tuc.ifi.ipsse.tuvexample.mutants.ACC;
/**
 * 
 * this class is used as a datastructure which bundles a testcase and an instance of the acc 
 * on which the testcase is executed
 *
 */
public class MutantBundle {
	private ACC acc;
	private ArrayList<Var> varList;
	public MutantBundle(ACC acc,ArrayList<Var> varList){
		this.acc = acc;
		this.varList = varList;
	}
	public ACC getAcc() {
		return acc;
	}
	public ArrayList<Var> getVarList() {
		return varList;
	}
	
}
