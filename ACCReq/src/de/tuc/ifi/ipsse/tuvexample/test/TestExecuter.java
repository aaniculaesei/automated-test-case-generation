package de.tuc.ifi.ipsse.tuvexample.test;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import de.tuc.ifi.ipsse.tuvexample.mutants.ACC;
/*
 * this class takes the filename of a trace list and executes the test cases
 */
@RunWith(Parameterized.class)  
public class TestExecuter {  
	//helper is used for the execution and preparation of a testcase
    private static TestHelper helper;
    private ArrayList<Var> testcase;
    //filename of the trace list
    private static String filename = "req.txt";
	@Parameters  
    public static ArrayList<MutantBundle>
                               generateParams() {  
                               	ArrayList<ArrayList<Var>> testsuite = new ArrayList<>();
                               	//list containing all testcases for every mutant
                               	ArrayList<MutantBundle> bundle = new ArrayList<>();
                               	try {
//                               		ACC original = (ACC) Class.forName("de.tuc.ifi.ipsse.tuvexample.mutants.StateMachineACCOri").newInstance();                               		
//                               		TestHelper helper1 = new TestHelper(filename,original);
//                               		testsuite = helper1.readTestsuite();
//									testsuite = reduceTestsuite(testsuite);
//									for(ArrayList<Var> tc:testsuite){
//										bundle.add(new MutantBundle(original, tc));
//									}
                               		
                               		// create a mutant bundle for every testcase and every mutant
									int numMutants = 0;
									for(int i=1;i<=numMutants;i++){
										ACC mutant = (ACC) Class.forName("de.tuc.ifi.ipsse.tuvexample.mutants.Mutant"+String.valueOf(i)).newInstance();
										TestHelper helper2 = new TestHelper(filename,mutant);
	                               		testsuite = helper2.readTestsuite();
										testsuite = reduceTestsuite(testsuite);
										for(ArrayList<Var> tc:testsuite){
											bundle.add(new MutantBundle(mutant, tc));
										}
									}
									
                               			
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}

                               	return bundle;
    }  
    
    
    //this method removes duplicate traces from a testsuite
	public static ArrayList<ArrayList<Var>> reduceTestsuite(ArrayList<ArrayList<Var>> testsuite){
		for(int i=0;i<testsuite.size();i++){
			ArrayList<Var> test = testsuite.get(i);
			for(int j=i+1;j<testsuite.size();j++){
				if(test.size() == testsuite.get(j).size()){
					ArrayList<Var> compareTest = testsuite.get(j);
					boolean equal = false;
					for(int k=0;k<test.size();k++){
						if(test.get(k).toString().equals(compareTest.get(k).toString())){
							equal = true;
						}else{
							equal = false;
							break;
						}
					}
					if(equal){
						testsuite.remove(compareTest);
					}
				}
			}
		}
		return testsuite;
	}


	public TestExecuter(MutantBundle bundle) {
		this.helper = new TestHelper(filename, bundle.getAcc());
		this.testcase = helper.prepareTestcase(bundle.getVarList());
	}
     //execute the testcase on the mutant
     @Test  
     public void test() {  
    	 helper.executeTest(testcase);
    	 //if mutant is undetected (so failure or exception in testcase) -> print name for result analysis
    	 System.out.println(helper.getACC().getClass().getName());
     }  
}  