package de.tuc.ifi.ipsse.tuvexample.test;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import de.tuc.ifi.ipsse.tuvexample.mutants.ACC;
import de.tuc.ifi.ipsse.tuvexample.mutants.ACC.States;
import de.tuc.ifi.ipsse.tuvexample.mutants.StateMachineACCOri;

/*
 * this class is used to create the test cases
 */
public class TestHelper {	
	private ACC acc;
	private BufferedReader br;
	private HashMap<String, VarInfo> info = new HashMap<>(); 
	private boolean initialBlock = true;
	private double v_curr = 0.0;
	private double distVel = 0.0;
	private States lastState = null;
	public TestHelper(String filename, ACC acc) {
		// TODO Auto-generated constructor stub
		try {
			this.acc = acc;
			br = new BufferedReader(new FileReader(filename));
			// connect variable names in the implementation with the var names of the model checker
			info.put("ac.state", new VarInfo("state", States.class));
			info.put("ac.v_current", new VarInfo("inCurrentVelocity", Double.class));
			info.put("ac.v_cruise", new VarInfo("v_cruise", Double.class));
			info.put("ac.d_cruise", new VarInfo("d_cruise", Double.class));
			info.put("ac.warning", new VarInfo("outWarning", Boolean.class));
			info.put("ac.activate", new VarInfo("inActive", Boolean.class));
			info.put("ac.active", new VarInfo("outActive", Boolean.class));
			info.put("ac.brake", new VarInfo("inBreakPedal", Double.class));
			info.put("ac.accel", new VarInfo("inAcceleratorPedal", Double.class));
			info.put("ac.v_vif", new VarInfo("inObjectRelativeVelocity", Double.class));
			info.put("ac.d_to", new VarInfo("inObjectDistance", Double.class));
			info.put("ac.vif", new VarInfo("inObjectInFront", Boolean.class));
					

			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//reads a testsuite from a trace
	public ArrayList<ArrayList<Var>> readTestsuite() throws IOException{
		ArrayList<Var> varList = new ArrayList<>();	
		ArrayList<ArrayList<Var>> testsuite = new ArrayList<>();	
		String line = br.readLine();
		boolean first = true;
		while(line != null){
			if(line.contains("specification") & !first){
				Var var = new Var(acc,false,null,"new testcase");
				varList.add(var);
				testsuite.add(varList);
				varList = new ArrayList<>();	
			}
			first = false;
			if(line.contains(".1 ")){		
				initialBlock = true;
			}else if(line.contains("State:")){	
				initialBlock = false;
				Var var = new Var(acc, false, null, "statechange");
				varList.add(var);
			}
			if(line.contains("ac.")){			
				String[] split = line.split(" = ");
				split[0] = split[0].trim();
				split[1] = split[1].trim();

				VarInfo varInfo = info.get(split[0]);
				if(varInfo!=null){
					if(split[0].contains("v_current")){		
						v_curr = Double.parseDouble(split[1])/3.6;
						Var var = new Var(acc, initialBlock, (v_curr), varInfo.getVarNameAcc());
						varList.add(var);
					}
					else if(split[0].contains("v_vif")){	
						Var var = new Var(acc, initialBlock, (Double.parseDouble(split[1])/3.6)-(v_curr), varInfo.getVarNameAcc());
						varList.add(var);
					}
					else if(split[0].contains("v_cruise")){	
						Var var = new Var(acc, initialBlock, Double.parseDouble(split[1])/3.6, varInfo.getVarNameAcc());
						varList.add(var);
					}
					else{			
						Var var = new Var(acc, initialBlock,stringToObject(split[1], varInfo.getType()), varInfo.getVarNameAcc());
						varList.add(var);
					}
					
				}
			}
			
			line = br.readLine();
		}
		testsuite.add(varList);
		return testsuite;
	}
	/**
	 * defines if a var is an oracle or an input variable for a testcase
	 */
	public ArrayList<Var> prepareTestcase(ArrayList<Var> varList){
		for(int i=0;i<varList.size();i++){
			if(!varList.get(i).isInitState() & !(varList.get(i).getVarNameAcc().equals("inAcceleratorPedal") | varList.get(i).getVarNameAcc().equals("inBreakPedal") | varList.get(i).getVarNameAcc().equals("inActive") | varList.get(i).getVarNameAcc().equals("statechange") | varList.get(i).getVarNameAcc().equals("new testcase"))){
				varList.get(i).setCheck(true);
			}
			if(!varList.get(i).isInitState() & varList.get(i).getVarNameAcc().equals("outActive") ){
				varList.get(i).setCheck(true);
				if(varList.get(i+1).getVarNameAcc().equals("statechange") & !varList.get(i-1).getVarNameAcc().equals("inBreakePedal")){
					varList.remove(i+1);	
				}
			}
			if(!varList.get(i).isInitState() & varList.get(i).getVarNameAcc().equals("inObjectInFront") ){
				varList.get(i).setCheck(false);
			}
		}
		return varList;
	}
	
	/**
	 * 
	 * executes the testcase
	 */
	public void executeTest(ArrayList<Var> varList){
		for(int i=0;i<varList.size();i++){
			if(varList.get(i).getVarNameAcc().equals("statechange")){
				acc.step(0.1);					
			}
			else if(varList.get(i).getVarNameAcc().equals("new testcase")){
//				System.out.println("----- Neuer Testfall -----");
			}else{
				if(!varList.get(i).isCheck()){
					varList.get(i).set();	//set value in acc
				}else{
					varList.get(i).check();	//check java acc value against smv value		
				}
				
			}
			/**
			 * regulation behaviour for velocities and distances of the model
			 */
			if(varList.get(i).getVarNameAcc().equals("inObjectDistance") && acc.state.equals(States.ON_DISTANCE) && acc.inObjectDistance == acc.getDistance().cruiseDistVIF && acc.inObjectRelativeVelocity==0){
				acc.inObjectDistance = acc.d_cruise;
				acc.inCurrentVelocity = acc.v_cruise;
			}

			if(varList.get(i).getVarNameAcc().equals("state")){
				States currentState = (States)varList.get(i).getValue();
				if(currentState == States.ON_SPEED && lastState == States.ON_DISTANCE){
					acc.inCurrentVelocity = acc.v_cruise;
					acc.inObjectDistance = acc.d_cruise;
				}
				if(varList.get(i).getValue().equals(States.ON_DISTANCE)){ 
						acc.inCurrentVelocity = acc.inCurrentVelocity + acc.inObjectRelativeVelocity;	
				        acc.inObjectDistance = (acc.inCurrentVelocity*3.6)/2;   
				        acc.inObjectRelativeVelocity = 0;
				        acc.updateWarning();
				        lastState = States.ON_DISTANCE;
				        distVel = acc.inCurrentVelocity;   
				}
				if(varList.get(i).getValue().equals(States.OFF) & !varList.get(i).isInitState()){
					lastState = States.OFF;
					acc.inCurrentVelocity = distVel;				
					acc.inBreakPedal = 0.0;
					acc.inAcceleratorPedal = 0.0;
					acc.inActive = true;
				}
				
				
			}
		}
		acc.step(0.1);
	}


	// this method converts the datatypes of the model into datatypes used by the java acc
	public Object stringToObject(String string,Class type){
		if(type == Double.class){
			if(string.contains("FALSE")){
				return 0.0;
			}else if(string.contains("TRUE")){
				return 1.0;
			}else{
				return Double.parseDouble(string);
			}
			
		}
		if(type == Boolean.class){
			return Boolean.parseBoolean(string);
		}
		if(type == States.class){
			switch(string){
			case "off":
				return States.OFF;
			case "speed":
				return States.ON_SPEED;
			case "distance":
				return States.ON_DISTANCE;
			}
		}
	
		return null;
	}
	public ACC getACC(){
		return acc;
	}

}
