package de.tuc.ifi.ipsse.tuvexample.test;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
/**
 * 
 * this class counts how many mutants were killed by a testsuite
 */
public class ResultCalculator {
	public static void main(String[] args) throws Exception {
		//total number of mutants
		//filenames of the testcase results
		//number of test cases in each testsuite
		int numMutants = 524;
		String reqFilename = "reqres";
		int numReqTC = 6;
		String anteFilename = "anteres";
		int numAnteTC = 7;	//18
		String ufcFilename = "ufcres";
		int numUFCtc = 18;	//18
		
		System.out.println("Ergebnis req Testsuite:");
		System.out.println(countLivingMutants(reqFilename, numReqTC, numMutants));
		System.out.println("Ergebnis ante Testsuite:");
		System.out.println(countLivingMutants(anteFilename, numAnteTC, numMutants));
		System.out.println("Ergebnis UFC Testsuite:");
		System.out.println(countLivingMutants(ufcFilename, numUFCtc, numMutants));

	}
	
	public static String countLivingMutants(String filename,int numTC,int numMutants) throws Exception{
		BufferedReader br = new BufferedReader(new FileReader(filename));
		String line = "";
		//get names of undetected mutants
		ArrayList<String> names = new ArrayList<>();
		while( (line=br.readLine())!=null){
			String[] split = line.split("mutants.");
			names.add(split[split.length-1]);	
		}
		//check if a mutant is not killed
		ArrayList<String> livingMutants = new ArrayList<>();
		for(int i=0;i<names.size();i++){
			int occurrences = Collections.frequency(names, names.get(i));
			if(occurrences==numTC){	//mutant is not killed
				if(!livingMutants.contains(names.get(i)))
					livingMutants.add(names.get(i));
			}
		}
		//calculation of killed mutants
		System.out.println("Anzahl lebend:"+livingMutants.size());
		double a = livingMutants.size();
		double b = numMutants;
		double c = a/b;
		System.out.println("Gekillte Mutanten: "+(1-c));
		return String.valueOf(1-c);
	}
}
