/*
 * Copyright 2016 Clausthal Univesity of Technology.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.tuc.ifi.ipsse.tuvexample.simulator;

import javax.swing.JFrame;
import javax.swing.JOptionPane;


import de.tuc.ifi.ipsse.tuvexample.acc_statemachine.StateMachineACC;
import de.tuc.ifi.ipsse.tuvexample.gui.Mode;
import de.tuc.ifi.ipsse.tuvexample.gui.SimulatorPanel;

/**
 *
 * @author falk
 */
public class RealTimeSimulator extends JFrame {
    
    private final SimulatorPanel ui = new SimulatorPanel();
    
    private final StateMachineACC acc = new StateMachineACC();
    
    private final GameLoop loop = new GameLoop();
    
    private RealTimeSimulator() {
        setContentPane(ui);
        pack();
        setLocationRelativeTo(null);
        setResizable(false);
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);        
        setVisible(true);               
    }          
    
    private void start() {
       Thread t = new Thread(loop);
        t.start(); 
    }
            
    private class GameLoop implements Runnable {

        double egoVelocityInMS = 0.0;
        double egoPosInM = 0.0;
        double carPosInM = 0.0;
        double carVelocityInMS = 0.0;
        boolean hasCar = false;
        boolean crash = false;
        
        @Override
        public void run() {
            long last = System.currentTimeMillis();
            
            while (true) {

                try {
                    Thread.sleep(100);
                } catch (InterruptedException ex) {
                    //
                }
                
                if (!isVisible()) {
                    dispose();
                    break;
                }
                
                Mode mode = ui.getMode();
                if (mode == Mode.PAUSE) {
                    last = System.currentTimeMillis();
                    continue;
                }

                // simulate world
                long time = System.currentTimeMillis();                
                double dt = ((double) (time-last)) / 1000.0;  
                
                double oldEgoPosInM = egoPosInM;
                egoPosInM += egoVelocityInMS * dt;
                
                if (hasCar) {
                    double oldCarPosInM = carPosInM;
                    carPosInM += carVelocityInMS * dt;
                    
                    if (Math.abs(carPosInM - egoPosInM) < 2.5 ||
                            (oldCarPosInM > oldEgoPosInM && carPosInM < egoPosInM)) {
                        crash = true; 
                    }
                }                    
                
                if (crash) {
                    JOptionPane.showMessageDialog(null, "BOOOM!!!");
                    this.hasCar = false;
                    this.crash = false;                    
                    ui.setHasCar(false);
                }
                
                // read gui
                if (ui.hasCar()) {                    
                    this.carVelocityInMS = ui.getCarVelocityInMS();                   
                    if (!this.hasCar) {
                        this.hasCar = true;
                        this.carPosInM = egoPosInM + ui.getDistanceInM();
                    }                    
                } else {
                    this.hasCar = false;
                }
                
                acc.inAcceleratorPedal = ui.getAcceleration();
                acc.inBreakPedal = ui.getDeceleration();
                                         
                acc.inActive = ui.getAccActive();                        
                                                            
                // simulate acc
                acc.inObjectInFront = hasCar;
                acc.inObjectRelativeVelocity = carVelocityInMS - egoVelocityInMS;                
                acc.inObjectDistance = carPosInM - egoPosInM;                
                
                acc.inCurrentVelocity = egoVelocityInMS;

                // simulate acc
              
                acc.step(dt);                
                last = time;
                
                // update gui                
                this.egoVelocityInMS = acc.outVelocity;
                ui.setAccActive(acc.outActive);
                ui.setAccSpeed(acc.getCCVelocity());
                ui.setWarning(acc.outWarning);
                ui.setVelocity(this.egoVelocityInMS);
                ui.setEgoPos(this.egoPosInM);
                
                ui.setDrawCar(hasCar);
                ui.setCarPos(this.carPosInM);
                
                ui.update();
                        
                    
            }
            System.exit(0);
        }
        
    }
    
    public static void main(String[] args) {
        RealTimeSimulator rt = new RealTimeSimulator();
        rt.start();
    }
    
}
